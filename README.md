# Patched `dwm`

Fork of `dwm` - *dynamic window manager for X* from 
[`suckless.org`](http://suckless.org).

```bash
sudo make install clean
```

## Patches applyed

- [`noborder`](http://dwm.suckless.org/patches/noborder/)
- [`pertag`](http://dwm.suckless.org/patches/pertag/)
- [`alwayscenter`](http://dwm.suckless.org/patches/alwayscenter/)

## Configs

- `Mod4` as the *Super Key*
- `resizehints=0`
